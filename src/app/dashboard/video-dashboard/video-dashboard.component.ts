import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from 'src/app/interfaces';
import { VideoDataService } from 'src/app/video-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;

  constructor(
    svc: VideoDataService,
    route: ActivatedRoute,
    router: Router
  ) {
    this.videos = svc.videos$.pipe(tap(videos => {
      if (videos.length &&
        !route.snapshot.queryParamMap.get('selectedId')) {
          router.navigate([], {
            queryParams: {
              selectedId: videos[0].id
            }
          });
      }
    }));
  }
}
