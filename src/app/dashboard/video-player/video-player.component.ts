import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { Video } from 'src/app/interfaces';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  video: Observable<Video>;
  url: Observable<SafeUrl>;

  constructor(
    route: ActivatedRoute,
    svc: VideoDataService,
    sanitizer: DomSanitizer
  ) {
    this.video = route.queryParamMap.pipe(
      map(params => params.get('selectedId')),
      filter(id => !!id),
      switchMap(id => svc.getVideo(id))
    );
    this.url = this.video.pipe(
      map(video => {
        return sanitizer.bypassSecurityTrustResourceUrl(
          'https://www.youtube.com/embed/' + video.id
        );
      })
    );
  }
}
