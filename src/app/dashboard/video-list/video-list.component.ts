import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Video } from 'src/app/interfaces';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  selectedVideoId: Observable<string>;

  constructor(route: ActivatedRoute, private router: Router) {
    this.selectedVideoId = route.queryParamMap.pipe(
      map(params => params.get('selectedId'))
    );
  }

  selectVideo(video: Video): void {
    this.router.navigate([], {
      queryParams: {
        selectedId: video.id
      }
    });
  }

}
