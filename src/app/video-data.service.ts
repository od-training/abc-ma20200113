import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Video } from './interfaces';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  videos$: Observable<Video[]>;

  constructor(private http: HttpClient) {
    this.videos$ = http.get<Video[]>(apiUrl + '/videos').pipe(
      map(videos => {
        return videos
          // include only the videos that start with 'Angular'
          .filter(angularFilter)
          // use the spread operator to create a shallow
          // clone, then change the title to all-caps
          .map(video => ({ ...video, title: video.title.toUpperCase() })
          );
      })
    );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(apiUrl + '/videos/' + id);
  }
}

function angularFilter(video: Video) {
  return video.title.startsWith('Angular');
}
