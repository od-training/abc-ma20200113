# Angular Boot Camp

[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager](http://videomanager.angularbootcamp.com)

[Workshop Repository](https://bitbucket.org/od-training/abc-ma20200113)

[Web Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Video Data](https://api.angularbootcamp.com/videos)

[Class Survey](https://angularbootcamp.com/survey)

## VSCode Extensions

[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[angular2-inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)

[angular2-switcher](https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher)

[Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

[Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

[TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

## Resources

[Nx (library builder atop the CLI)](https://nx.dev/angular)

[Bill Odom: CSS Grid](https://www.youtube.com/watch?v=lh6n0JxXD_g)

## Observables

[Cory Rylan Video on Observables](https://www.youtube.com/watch?v=-yY2ECd2tSM)

[Ben Lesh Video on creating Observables](https://www.youtube.com/watch?v=m40cF91F8_A)

[Book: Build Reactive Websites with RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

[I switched a map and you'll never guess what happened next (SwitchMap video)](https://www.youtube.com/watch?v=rUZ9CjcaCEw)

[RXMarbles](http://rxmarbles.com)

[LearnRXJS](https://www.learnrxjs.io/)

[Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)

[Reactive Visualizations](https://reactive.how/)

[Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)

["Crying Baby" Video from Kyle Cordes on State Management](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

[Component Interaction](https://angular.io/guide/component-interaction)

## Conferences

[Angular Conferences](https://angularconferences.com/)

## Podcasts/Videocasts

[Angular Air (Videocast)](https://angularair.com/)

[Adventures in Angular (Podcast)](https://devchat.tv/adv-in-angular)

[Real Talk JavaScript](https://realtalkjavascript.simplecast.fm/)

[Code Talk Teach (Angular Boot Camp curriculum discussions)](https://www.youtube.com/channel/UCC3ihTPwdU0PU9P7QGJa7bw)

## Newsletters

[ng-newsletter (weekly Angular email)](https://www.ng-newsletter.com/)

[Angular Top 5 (weekly Angular email)](http://angulartop5.com/)

## Blogs

[Angular In Depth](https://blog.angularindepth.com/)

[Netanel Basal](https://netbasal.com/)

[Cory Rylan](https://coryrylan.com/)

## Utilities

[json2ts (generate TypeScript interfaces from JSON)](http://json2ts.com/)

[Can I Use?](https://caniuse.com/)

[Moment JS (Date library)](https://momentjs.com/)

[date-fns (Date library)](https://date-fns.org/)
